package com.b2w.starwars.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.b2w.starwars.exception.PlanetNotFoundException;
import com.b2w.starwars.model.Planet;
import com.b2w.starwars.repository.PlanetRepository;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class PlanetServiceTest {

    private static final String PLANET_ID = UUID.randomUUID().toString();
    private static final String PLANET_NAME = "Tatooine";
    private static final String PLANET_CLIMATE = "arid";
    private static final String PLANET_TERRAIN = "desert";
    private static final Integer PLANET_FILM_NUM = 5;

    private static final String INSERT_PLANET_ID = UUID.randomUUID().toString();
    private static final String INSERT_PLANET_NAME = "Hoth";
    private static final String INSERT_PLANET_CLIMATE = "frozen";
    private static final String INSERT_PLANET_TERRAIN = "tundra";
    private static final Integer INSERT_PLANET_FILM_NUM = 1;

    @Autowired
    @InjectMocks
    private PlanetService planetService;

    @Mock
    private PlanetRepository planetRepository;

    @Test
    void testServiceExists() {
        assertNotNull(planetService);
    }

    @Test
    void testSaveNewPlanet() {
        Planet planet = new Planet(null, PLANET_NAME, PLANET_CLIMATE, PLANET_TERRAIN, null);
        when(planetRepository.save(any(Planet.class))).thenReturn(planet);
        assertEquals(planet, planetService.save(planet));
    }

    @Test
    void testFindPlanetByName() {
        Planet planet = new Planet(PLANET_ID, PLANET_NAME, PLANET_CLIMATE, PLANET_TERRAIN, PLANET_FILM_NUM);
        when(planetRepository.findByName(PLANET_NAME)).thenReturn(Optional.of(planet));
        assertEquals(planet, planetService.findByName(PLANET_NAME));
    }

    @Test
    void testCantFindPlanetByName() {
        when(planetRepository.findByName(PLANET_NAME)).thenReturn(Optional.empty());
        assertThrows(PlanetNotFoundException.class, () -> planetService.findByName(PLANET_NAME));
    }
 
    @Test
    void testFindPlanetById() {
        Planet planet = new Planet(PLANET_ID, PLANET_NAME, PLANET_CLIMATE, PLANET_TERRAIN, PLANET_FILM_NUM);
        when(planetRepository.findById(PLANET_ID)).thenReturn(Optional.of(planet));
        assertEquals(planet, planetService.findById(PLANET_ID));
    }

    @Test
    void testCantFindPlanetById() {
        Planet planet = new Planet(PLANET_ID, PLANET_NAME, PLANET_CLIMATE, PLANET_TERRAIN, PLANET_FILM_NUM);
        when(planetRepository.findById(PLANET_ID)).thenReturn(Optional.empty());
        assertThrows(PlanetNotFoundException.class, () -> planetService.findById(PLANET_ID));
    }

    @Test
    void testFindAllPlanets() {
        Planet planet1 = new Planet(PLANET_ID, PLANET_NAME, PLANET_CLIMATE, PLANET_TERRAIN, PLANET_FILM_NUM);
        Planet planet2 = new Planet(INSERT_PLANET_ID, INSERT_PLANET_NAME, INSERT_PLANET_CLIMATE, INSERT_PLANET_TERRAIN, INSERT_PLANET_FILM_NUM);
        List<Planet> list = Arrays.asList(planet1, planet2);
        when(planetRepository.findAll()).thenReturn(list);
        assertThat(planetService.findAll(), contains(planet1, planet2));
    }

    @Test
    void testPlanetListEmpty() {
        when(planetRepository.findAll()).thenReturn(Collections.emptyList());
        assertTrue(planetService.findAll().isEmpty());
    }

}