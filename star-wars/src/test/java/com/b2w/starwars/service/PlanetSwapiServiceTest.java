package com.b2w.starwars.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.b2w.starwars.model.PlanetResultsSwapi;
import com.b2w.starwars.model.PlanetsSwapi;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class PlanetSwapiServiceTest {

    private static final Integer NUM_FILMS = 5;
    private static final String PLANET_NAME = "Tatooine";
    private static final String URL = "https://swapi.dev/api/planets/?search=";
 
    @Autowired
    @InjectMocks
    private PlanetSwapiService planetSwapiService;
    
    @Mock
    private RestTemplate restTemplate;

    private PlanetResultsSwapi getSwapiResults() {
        PlanetResultsSwapi planetsResultsSwapi = new PlanetResultsSwapi();
        planetsResultsSwapi.setName(PLANET_NAME);
        List<String> films = new ArrayList<String>();
        films.add("http://swapi.dev/api/films/1/");
        films.add("http://swapi.dev/api/films/3/");
        films.add("http://swapi.dev/api/films/4/");
        films.add("http://swapi.dev/api/films/5/");
        films.add("http://swapi.dev/api/films/6/");
        planetsResultsSwapi.setFilms(films);
        return planetsResultsSwapi;
    }

    private PlanetsSwapi getSwapiObject() {
        PlanetsSwapi planetsSwapi = new PlanetsSwapi();
        List<PlanetResultsSwapi> listResults = new ArrayList<PlanetResultsSwapi>();
        listResults.add(getSwapiResults());
        planetsSwapi.setResults(listResults);
        return planetsSwapi;
    }

    @Test
    void testGetNumFilms() {        
        when(restTemplate.getForObject(URL + PLANET_NAME, PlanetsSwapi.class)).thenReturn(getSwapiObject());
        assertEquals(NUM_FILMS, planetSwapiService.getNumFilms(PLANET_NAME));
    }
}