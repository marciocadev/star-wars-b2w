package com.b2w.starwars.controller;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import com.b2w.starwars.exception.PlanetDuplicateException;
import com.b2w.starwars.exception.PlanetNotFoundException;
import com.b2w.starwars.model.Planet;
import com.b2w.starwars.service.PlanetService;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(PlanetController.class)
public class PlanetControllerTest {

    private static final String PLANET_ID = UUID.randomUUID().toString();
    private static final String PLANET_NAME = "Tatooine";
    private static final String PLANET_CLIMATE = "arid";
    private static final String PLANET_TERRAIN = "desert";
    private static final Integer PLANET_FILM_NUM = 5;

    private static final String INSERT_PLANET_ID = UUID.randomUUID().toString();
    private static final String INSERT_PLANET_NAME = "Hoth";
    private static final String INSERT_PLANET_CLIMATE = "frozen";
    private static final String INSERT_PLANET_TERRAIN = "tundra";
    private static final Integer INSERT_PLANET_FILM_NUM = 1;
    
    @Autowired
    private MockMvc mockMvc;
    
    @MockBean
    private PlanetService planetService;

    @Test
    void testInsertNewPlanet() throws Exception {
        Planet planet = new Planet(PLANET_ID, PLANET_NAME, PLANET_CLIMATE, PLANET_TERRAIN, PLANET_FILM_NUM);
        mockMvc
            .perform(post("/planet")
            .content(new ObjectMapper().writeValueAsString(planet))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());
    }

    @Test
    void testInsertDuplicatedPlanet() throws Exception {
        Planet planet = new Planet(PLANET_ID, PLANET_NAME, PLANET_CLIMATE, PLANET_TERRAIN, PLANET_FILM_NUM);
        when(planetService.save(planet)).thenThrow(new PlanetDuplicateException("Planet", "name", PLANET_NAME));
        mockMvc
            .perform(post("/planet")
            .content(new ObjectMapper().writeValueAsString(planet))
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isConflict());
    }

    @Test
    void testDeletePlanet() throws Exception {
        mockMvc
            .perform(delete("/planet/{id}", PLANET_ID))
            .andExpect(status().isOk());
    }

    @Test
    void testFindPlanetByName() throws Exception {
        Planet planet = new Planet(PLANET_ID, PLANET_NAME, PLANET_CLIMATE, PLANET_TERRAIN, PLANET_FILM_NUM);
        when(planetService.findByName(PLANET_NAME)).thenReturn(planet);
        mockMvc
            .perform(get("/planet").param("name", PLANET_NAME))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString(PLANET_ID)))
            .andExpect(content().string(containsString(PLANET_NAME)))
            .andExpect(content().string(containsString(PLANET_CLIMATE)))
            .andExpect(content().string(containsString(PLANET_TERRAIN)));
    }

    @Test
    void testCantFindPlanetByName() throws Exception {
        when(planetService.findByName(PLANET_NAME)).thenThrow(new PlanetNotFoundException("Planet", "name", PLANET_NAME));
        mockMvc
            .perform(get("/planet").param("name", PLANET_NAME))
            .andExpect(status().isNotFound());
    }

    @Test
    void testFindPlanetById() throws Exception {
        Planet planet = new Planet(PLANET_ID, PLANET_NAME, PLANET_CLIMATE, PLANET_TERRAIN, PLANET_FILM_NUM);
        when(planetService.findById(PLANET_ID)).thenReturn(planet);
        mockMvc
            .perform(get("/planet/{id}", PLANET_ID))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString(PLANET_ID)))
            .andExpect(content().string(containsString(PLANET_NAME)))
            .andExpect(content().string(containsString(PLANET_CLIMATE)))
            .andExpect(content().string(containsString(PLANET_TERRAIN)));
    }
   
    @Test
    void testCantFindPlanetById() throws Exception {
        when(planetService.findById(PLANET_ID)).thenThrow(new PlanetNotFoundException("Planet", "id", PLANET_ID));
        mockMvc
            .perform(get("/planet/{id}", PLANET_ID))
            .andExpect(status().isNotFound());
    }

    @Test
    void testFindAllPlanets() throws Exception {
        Planet planet1 = new Planet(PLANET_ID, PLANET_NAME, PLANET_CLIMATE, PLANET_TERRAIN, PLANET_FILM_NUM);
        Planet planet2 = new Planet(INSERT_PLANET_ID, INSERT_PLANET_NAME, INSERT_PLANET_CLIMATE, INSERT_PLANET_TERRAIN, INSERT_PLANET_FILM_NUM);
        List<Planet> list = Arrays.asList(planet1, planet2);
        when(planetService.findAll()).thenReturn(list);
        mockMvc
            .perform(get("/planet/listAll"))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString(PLANET_NAME)))
            .andExpect(content().string(containsString(INSERT_PLANET_NAME)));
    }
}