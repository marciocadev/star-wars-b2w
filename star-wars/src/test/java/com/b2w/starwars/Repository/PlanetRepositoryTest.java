package com.b2w.starwars.repository;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.b2w.starwars.exception.PlanetDuplicateException;
import com.b2w.starwars.exception.PlanetNotFoundException;
import com.b2w.starwars.model.Planet;
import com.b2w.starwars.repository.PlanetRepository;
import com.mongodb.DuplicateKeyException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class PlanetRepositoryTest {

    private static final String PLANET_NOT_FOUND_BY_ID = UUID.randomUUID().toString();

    private static final String PLANET_NOT_FOUND_BY_NAME = "Dagobah";

    private static final String PLANET_ID = UUID.randomUUID().toString();
    private static final String PLANET_NAME = "Tatooine";
    private static final String PLANET_CLIMATE = "arid";
    private static final String PLANET_TERRAIN = "desert";
    private static final Integer PLANET_FILM_NUM = 5;

    private static final String INSERT_PLANET_ID = UUID.randomUUID().toString();
    private static final String INSERT_PLANET_NAME = "Hoth";
    private static final String INSERT_PLANET_CLIMATE = "frozen";
    private static final String INSERT_PLANET_TERRAIN = "tundra";
    private static final Integer INSERT_PLANET_FILM_NUM = 1;
    
    @Autowired
    private PlanetRepository planetRepository;

    @Test
    @DisplayName("Testa se p repositorio está instanciado")
    void testPlanetRepositoryExist() {
        assertNotNull(planetRepository);
    }

    @BeforeEach
    void beforeEachTest() {
        Planet newPlanet = new Planet(PLANET_ID, PLANET_NAME, PLANET_CLIMATE, PLANET_TERRAIN, PLANET_FILM_NUM);
        planetRepository.save(newPlanet);
    }

    @AfterEach
    void afterEachTest() {
        planetRepository.deleteAll();
    }

    @Test
    void testFindPlanetByName() {
        Planet planet = new Planet(PLANET_ID, PLANET_NAME, PLANET_CLIMATE, PLANET_TERRAIN, PLANET_FILM_NUM);
        assertEquals(Optional.of(planet), planetRepository.findByName(PLANET_NAME));
    }

    @Test
    void testCantFindPlanetByName() {
        assertThrows(
            PlanetNotFoundException.class, 
            () -> {
                planetRepository.findByName(PLANET_NOT_FOUND_BY_NAME)
                                .orElseThrow(() -> new PlanetNotFoundException("Planet", "name", PLANET_NOT_FOUND_BY_NAME));
            });
    }

    @Test
    void testFindPlanetById() {        
        Planet planet = new Planet(PLANET_ID, PLANET_NAME, PLANET_CLIMATE, PLANET_TERRAIN, PLANET_FILM_NUM);
        assertEquals(Optional.of(planet), planetRepository.findById(PLANET_ID));       
    }

    @Test
    void testCantFindPlanetById() {
        assertThrows(
            PlanetNotFoundException.class, 
            () -> {
                planetRepository.findById(PLANET_NOT_FOUND_BY_ID)
                                .orElseThrow(() -> new PlanetNotFoundException("Planet", "id", PLANET_NOT_FOUND_BY_ID));
            }
        );
    }

    @Test
    void testInserNewPlanetOnMongo() {
        Planet newPlanet = new Planet(INSERT_PLANET_ID, INSERT_PLANET_NAME, INSERT_PLANET_CLIMATE, INSERT_PLANET_TERRAIN, INSERT_PLANET_FILM_NUM);
        assertEquals(
            new Planet(INSERT_PLANET_ID, INSERT_PLANET_NAME, INSERT_PLANET_CLIMATE, INSERT_PLANET_TERRAIN, INSERT_PLANET_FILM_NUM), 
            planetRepository.save(newPlanet)
        );
    }

    @Test
    void testInsertDuplicatedPlanet() {
        try {
            Planet newPlanet = new Planet(null, PLANET_NAME, INSERT_PLANET_CLIMATE, PLANET_TERRAIN, PLANET_FILM_NUM);
            planetRepository.save(newPlanet);
        }
        catch(Exception ex) {
            assertTrue(ex.getMessage().contains("duplicate key error"));
        }
    }

    @Test
    void testDeletePlanet() {
        Planet planet = new Planet(PLANET_ID, PLANET_NAME, INSERT_PLANET_CLIMATE, PLANET_TERRAIN, PLANET_FILM_NUM);
        planetRepository.delete(planet);
        assertEquals(Optional.empty(), planetRepository.findById(PLANET_ID));
    }

    @Test
    void testListAllPlanets() {
        Planet planet1 = new Planet(PLANET_ID, PLANET_NAME, PLANET_CLIMATE, PLANET_TERRAIN, PLANET_FILM_NUM);
        Planet planet2 = new Planet(INSERT_PLANET_ID, INSERT_PLANET_NAME, INSERT_PLANET_CLIMATE, INSERT_PLANET_TERRAIN, INSERT_PLANET_FILM_NUM);
        planetRepository.save(planet1);
        planetRepository.save(planet2);
        List<Planet> list = planetRepository.findAll();
        assertThat(planetRepository.findAll(), contains(planet1, planet2));
    }

    @Test
    void testPlanetCollectionEmpty() {
        planetRepository.deleteAll();
        assertTrue(planetRepository.findAll().isEmpty());
    }
}
