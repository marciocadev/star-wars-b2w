package com.b2w.starwars.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Data;

@ResponseStatus(value = HttpStatus.CONFLICT)
@Data
public class PlanetDuplicateException extends RuntimeException {

    private static final long serialVersionUID = -964903303626728090L;
    
    private String resourceName;
    private String fieldName;
    private Object fieldValue;

    public PlanetDuplicateException(String resourceName, String fieldName, Object fieldValue) {
        super(String.format("%s not found with %s : '%s'", resourceName, fieldName, fieldValue));
        this.resourceName = resourceName;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }
}