package com.b2w.starwars.repository;

import java.util.Optional;

import com.b2w.starwars.model.Planet;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PlanetRepository extends MongoRepository <Planet, String> {

    Optional<Planet> findByName(String name);
}