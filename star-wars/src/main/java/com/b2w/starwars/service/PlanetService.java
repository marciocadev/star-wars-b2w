package com.b2w.starwars.service;

import com.b2w.starwars.repository.PlanetRepository;

import java.util.List;

import com.b2w.starwars.exception.PlanetDuplicateException;
import com.b2w.starwars.exception.PlanetNotFoundException;
import com.b2w.starwars.model.Planet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PlanetService extends PlanetSwapiService {
    
    @Autowired
    private PlanetRepository planetRepository;

    @Transactional
    public Planet save(Planet planet) {
        try {
            planet.setNumFilms(getNumFilms(planet.getName()));
            return planetRepository.save(planet);
        }
        catch(Exception ex) {
            throw new PlanetDuplicateException("Planet", "name", planet.getName());
        }        
    }

    @Transactional
    public void delete(String id) {
        planetRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Planet findById(String id) {
        return planetRepository.findById(id).orElseThrow(() -> new PlanetNotFoundException("Planet", "id", id));
    }

    @Transactional(readOnly = true)
    public Planet findByName(String name) {
        return planetRepository.findByName(name).orElseThrow(() -> new PlanetNotFoundException("Planet", "name", name));
    }
    
    @Transactional(readOnly = true)
    public List<Planet> findAll() {
        return planetRepository.findAll();
    }
}