package com.b2w.starwars.service;

import com.b2w.starwars.model.PlanetResultsSwapi;
import com.b2w.starwars.model.PlanetsSwapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

public class PlanetSwapiService {

    private static String SWAPI_URL = "https://swapi.dev/api/planets/?search=";

    @Autowired
    private RestTemplate restTemplate;

    public Integer getNumFilms(String name) {
        PlanetsSwapi swapi = restTemplate.getForObject(SWAPI_URL + name, PlanetsSwapi.class);
        for (PlanetResultsSwapi item : swapi.getResults()) {
            if (name.toLowerCase().equals(item.getName().toLowerCase())) {
                return item.getFilms().size();
            }
        }
        return 0;
    }
}