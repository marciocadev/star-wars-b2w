package com.b2w.starwars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@PropertySource(value = {"classpath:db.properties"})
public class StarWarsApplication {

	@Bean
	public RestTemplate restTemplate() {
    	return new RestTemplate();
	}

	public static void main(String[] args) {
		SpringApplication.run(StarWarsApplication.class, args);
	}

}
