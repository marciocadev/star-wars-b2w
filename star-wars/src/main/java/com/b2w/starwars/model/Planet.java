package com.b2w.starwars.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Document
@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class Planet {
    @Id
    @ApiModelProperty(hidden = true)
    private String id;

    @Indexed(name = "name_index", unique = true, background = true)
    private String name; 

    private String climate;
    private String terrain;

    @ApiModelProperty(hidden = true)
    private Integer numFilms;
}