package com.b2w.starwars.controller;

import java.util.List;

import com.b2w.starwars.model.Planet;
import com.b2w.starwars.service.PlanetService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="planet")
@RestController
@RequestMapping("/planet")
public class PlanetController {

    @Autowired
    private PlanetService planetService;

    @ApiOperation(value = "Get all planets")
    @GetMapping("/listAll")
    public List<Planet> findAll() {
        return planetService.findAll();
    }

    @ApiOperation(value = "Find planet by id")
    @GetMapping("/{id}")
    public Planet findById(@PathVariable String id) {
        return planetService.findById(id);
    }

    @ApiOperation(value = "Find planet by name")
    @GetMapping
    public Planet findByName(@RequestParam String name) {
        return planetService.findByName(name);
    }

    @ApiOperation(value = "Insert new planet")
    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping
    public Planet insertPlanet(@RequestBody Planet newPlanet) {
        return planetService.save(newPlanet);
    }

    @ApiOperation(value = "Remove planet")
    @ResponseStatus(value = HttpStatus.OK)
    @DeleteMapping("/{id}")
    public void deletePlanet(@PathVariable String id) {
        planetService.delete(id);
    }
    
}