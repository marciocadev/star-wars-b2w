# Star Wars B2W

Este projeto foi criado utilizando **Java (Spring Boot)**, **MongoDB**, **Docker** e **Swagger**.

A opção por **Java** e **MongoDB** era uma premissa do teste.

Preferi utilizar o **Spring Boot** por ser um framework consolidado no mercado.

O **Docker** foi utilizado pela possibilidade de se ter dois ambientes isolados, um para a execução da aplicação e outro para testes, desta forma ao se executar os testes utilizamos o profile 'test' que executa os testes em um database no Mongo diferente da que será usada na aplicação.

A opção do **Swagger** foi para que se tivesse uma interface de teste do usuário e a possibilidade de se utilizar o OpenAPI que abre um leque de possibilidades, como gerar bibliotecas de acesso para linguagens de front entre outras coisas.

Caso não seja desejável utilizar o Docker, a aplicação não é dependente desse ambiente podendo ser executado normalmente, só tem de se configurar um banco Mongo e apontar no db.prod.properties e db.test.properties.

Como um plus eu coloquei o **Gitlab** atraves do gitlab-ci que a cada commit no projeto sejá executado os testes.

*Apesar de não ser especificado no teste, coloquei um índice no campo **"name"** do Planeta para se evitar duplicações de planetas com um mesmo nome, sei que não era uma premissa mas partindo da linha de pensamento que devemos nos comportar como **donos do projeto** não achei que era desejável uma repetição de nomes na base.*

## Construir o container de desenvolvimento
docker-compose build java-dev

## Executar o container de desenvolvimento em segundo plano no profile padrão e apontando para o database StarWars
docker-compose up -d java-dev

## Executar os casos de teste no com o profile 'test' e apontando para o database StarWarsTest
docker-compose run -rm java-dev ./mvnw -Ptest test

## Construir o container de produção
docker-compose build java-prod

## Colocar o container no ar
docker-compose up java-prod

## Para os serviços
docker-compose stop

## Swagger-ui
http://localhost:5001/star-wars-api/swagger-ui.html

## api-docs
http://localhost:5001/star-wars-api/v2/api-docs

## Mongo
* docker exec -it mongodb bash
* mongo
* show dbs
* use StarWarsTest
* show collections
* db.planet.find()
* db.planet.getIndexes()
